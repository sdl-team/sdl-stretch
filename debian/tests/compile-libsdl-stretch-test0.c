#include <SDL_stretch/SDL_stretch.h>

#include <stdio.h>

int main(int argc, char* argv[])
{
	const char* info = SDL_StretchInfo();
	fprintf(stdout, "SDL_StretchInfo: %s\n", info);

	return 0;
}
